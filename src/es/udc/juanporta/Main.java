package es.udc.juanporta;

import java.util.Scanner;

public class Main {

    private static final String[] OPERATIONS = new String[]{"1. Suma\n" +
            "   2. Resta\n" +
            "   3. Multiplicación\n" +
            "   4. División\n" +
            "   5. Doble\n"};

    public static void main(String[] args) {

        System.out.println("Selecciona una operación");
        for (String operation : OPERATIONS) {
            System.out.println("\t" + operation);
        }
        Scanner sc = new Scanner(System.in);
        String operationId = sc.nextLine();
        String a;
        String b;
        switch (operationId) {
            case "1":
                System.out.println("Inserte primer sumando");
                a = sc.nextLine();
                System.out.println("Inserte segundo sumando");
                b = sc.nextLine();
                try {
                    System.out.println("Resultado: " + new AddOperation().add(Float.parseFloat(a), Float.parseFloat(b)));
                } catch (NumberFormatException e) {
                    System.out.println("Ains... mete un número porfi... ఠ_ఠ");
                }
                break;

            case "2":
                System.out.println("Inserte primer dígito");
                a = sc.nextLine();
                System.out.println("Inserte segundo dígito");
                b = sc.nextLine();
                try {
                    System.out.println("Resultado: " + new AddOperation().resta(Float.parseFloat(a), Float.parseFloat(b)));
                } catch (NumberFormatException e) {
                    System.out.println("Ains... mete un número porfi... ఠ_ఠ");
                }
                break;

            case "3":
                System.out.println("Inserte primer dígito");
                a = sc.nextLine();
                System.out.println("Inserte segundo dígito");
                b = sc.nextLine();
                try {
                    System.out.println("Resultado: " + new AddOperation().multi(Float.parseFloat(a), Float.parseFloat(b)));
                } catch (NumberFormatException e) {
                    System.out.println("Ains... mete un número porfi... ఠ_ఠ");
                }
                break;

            case "4":
                System.out.println("Inserte primer dígito");
                a = sc.nextLine();
                System.out.println("Inserte segundo dígito");
                b = sc.nextLine();
                try {
                    System.out.println("Resultado: " + new AddOperation().div(Float.parseFloat(a), Float.parseFloat(b)));
                } catch (NumberFormatException e) {
                    System.out.println("Ains... mete un número porfi... ఠ_ఠ");
                }
                break;

            case "5":
                System.out.println("Inserte primer dígito");
                a = sc.nextLine();
                System.out.println("Inserte segundo dígito");
                b = sc.nextLine();
                try {
                    System.out.println("Resultado: " + new AddOperation().doble(Float.parseFloat(a), Float.parseFloat(b)));
                } catch (NumberFormatException e) {
                    System.out.println("Ains... mete un número porfi... ఠ_ఠ");
                }
                break;
            default:
                System.out.println("Operación no encontrada ¯\\_(ツ)_/¯");
        }

    }
}
