package es.udc.juanporta;

public class AddOperation {

    public float add(float a, float b) {

        return a + b;
    }

    public float resta(float a, float b) {
        return a - b;
    }

    public float multi(float a, float b) {
        return a * b;
    }

    public float div(float a, float b) {
        return a / b;
    }

    public float doble(float a, float b) {
        return (a+b)*2;
    }

}
